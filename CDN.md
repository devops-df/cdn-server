# Node.js CDN

This image have:

Expoxe every vendor lib on server URL root `/`
- server URL root (`http|s://<server>/...`)

e.g:

- AngularJS-1.5.8 (`/angular/...`)

  e.g.
  ```
  angular-csp.css  
  angular.js  
  angular.min.js  
  angular.min.js.map
  ```
- font-awesome-4.7.0 (`/font-awesome/...`)
- bootstrap-3.3.7 (`/bootstrap/...`)
- AngularJS-2.1.2 (`/@angular/...`)

... and so on

Packages installed are defined inside the [`package.json`](package.json) file on block `dependencies`, e.g.:

```json
"dependencies": {
  "express": ,
  "angular": ,
  "font-awesome": ,
  "bootstrap": ,
  "jquery":
  ...
}
```
