# CHANGELOG

## [v0.0.14](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.14) - add jquery-(ui, mobile) and some angular-1.5 modules

highlights:

- add some angular-1.5 modules, e.g: angular-resource, angular-route, angular-cookies
- add jquery-ui and jquery-mobile
- add CHANGELOG
- add CONTRIBUTING
- update *run it* instructions

## [v0.0.13](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.13) - Dynamic package version update

highlights:

- CI changes for dynamic package version update

## [v0.0.12](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.12) - Repository name changed (again) ;)

highlights:

- Repository changed from nodejs-cdn to cdn-server.
  - Update service name according to repo name
  - Update image registry path according to repo name

## [v0.0.11](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.11) - Repository name changed

highlights:

- Repository changed from nodejs-cdn-vendors to nodejs-cdn.
  - Update service name according to repo name
  - Update image registry path according to repo name

## [v0.0.10](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.10) - Add JQuery-3.1

highlights:

- add JQuery: ~3.1.0
- Update sample cdn-example/index.html
- Allow CORS for express.js

## [v0.0.9](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.9) - Add Angular-2.1

highlights:

- add angular: ~2.1.0
- add sample cdn-example/index.html
- expose all modules on cdn server

## [v0.0.8](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.8) - Node.js LTS (boron) based

highlights:

- Add bootstrap:~3.3.7 (see CDN.md)

## [v0.0.7](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.7) - Node.js LTS (boron) based

highlights:

- Add font-awesome-4.7.0 (see CDN.md)

## [v0.0.6](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.6) - Node.js LTS (boron) based

highlights:

- Node.js LTS (6.9.1-boron; npm-3.10.8) base image
- expose express http on internal port 8080
- Add angular-1.5.8 (see CDN.md)
- redefined cdn url

## [v0.0.5](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.5) - Node.js LTS (boron) based

highlights:

- Node.js LTS (6.9.1-boron; npm-3.10.8) base image
- expose express http on internal port 8080
- Add angular-1.5.8 (see CDN.md)

## [v0.0.4](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.4) - Node.js LTS (boron) based

highlights:

- Node.js LTS (6.9.1-boron; npm-3.10.8) base image
- expose express http on internal port 8080
- docker-composer file for production and development

## [v0.0.3](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.3) - NodeJS Wheezy

highlights:

- NodeJS LTS (7.0.0-wheezy; npm-3.10.8) base image
- expose http on internal port 8080

## [v0.0.2](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.2) - node boron

highlights:

- NodeJS LTS (6.9.1-boron; npm-3.10.8) base image
- expose http on internal port 8080

## [v0.0.1](https://gitlab.com/devopsdays-br/cdn-server/tags/v0.0.1) - initial version release

highlights:

- NodeJS LTS (4.6.1-argon; npm-2.15.9) base image
- expose http on internal port 8080
- CI image build
