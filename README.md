# CDN Conteinerized

#### Table of contents

1. [Overview](#overview)
2. [Technologies](#technologies)
3. [Authors](#authors)
4. [Contributors](#contributors)
5. [Compatibility](#compatibility)
6. [Requirements](#requirements)
7. [Setup and Tasting](#setup-and-tasting)
8. [What's this CDN has inside?](CDN.md)

## Overview

Conteinerizing vendors lib to work as a CDN.

### Vendors libs list

- font-awesome: 4.7
- bootstrap: 3.3
- jquery: 3.1
- jquery-ui: 1.12
- jquery-mobile: 1.4
- smartmenus: 1.0
- angular: 1.5
  - angular-resource: 1.5
  - angular-route: 1.5
  - angular-animate: 1.5
  - angular-cookies: 1.5
  - angular-sanitize: 1.5
  - angular-messages: 1.5
  - angular-aria: 1.5
  - angular-jwt: 0.1
  - angular-bootstrap: 0.12
  - angular-material: 1.1
  - angular-moment: 1.0
  - angular-ui-mask: 1.8
- angular2
  - @angular/common: 2.1
  - @angular/compiler: 2.1
  - @angular/core: 2.1
  - @angular/forms: 2.1
  - @angular/http: 2.1
  - @angular/platform-browser: 2.1
  - @angular/platform-browser-dynamic: 2.1
  - @angular/router: 3.1
  - @angular/upgrade: 2.1

#### Sample usage

```html
<!doctype html>
<html ng-app>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Adriano Vieira">

    <link href="http://cdn-server.hacklab/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://cdn-server.hacklab/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>

    <div>
      <label>Name:</label>
      <input type="text" ng-model="yourName" placeholder="Enter a name here">
      <hr>
      <h1><i class="fa fa-phone"></i> Hello {{yourName}}!</h1>
    </div>

    <script src="http://cdn-server.hacklab/angular/angular.min.js"></script>
  </body>
</html>
```

## Technologies

* Docker-1.12+
* Node.js LTS (boron)

## Authors

* Adriano Vieira (adriano.svieira at gmail.com)

## Contributors

* TBD

## Compatibility

Built on Node.js LTS image as base for production CDN.

### Tags

- **standard and supported**:
  - ***latest***: built based on node:boron image

## Requirements

- Docker-1.12+ <https://docker.com>

### Optionals
- VirtualBox-5.0+ <http://www.virtualbox.org/>
- Vagrant-1.7+ <http://vagrantup.com/>
  - Box ***`adrianovieira/centos7-docker1.12-GA`*** or ***`adrianovieira/boxes/centos7-kernel4.4-vbox5.0-docker1.12GA`*** (published on https://atlas.hashicorp.com/adrianovieira/boxes)

## Setup and Tasting

### Docker

First of all we need a host with Docker-1.12+ pre-installed and running. May be you could use one of my "Docker boxes" <https://atlas.hashicorp.com/adrianovieira/boxes> with built-in docker-1.12.

#### Docker Proxy

If you are behind a proxy and is having problems pulling docker imagens... setup *Systemd Docker own Service*, as below (well done on CEntOS-7):

```bash
# setup docker service proxy (as root user)
mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" > /etc/systemd/system/docker.service.d/proxy.conf
echo "Environment='HTTPS_PROXY=$HTTP_PROXY' 'HTTP_PROXY=$HTTP_PROXY'" >> /etc/systemd/system/docker.service.d/proxy.conf
systemctl daemon-reload && systemctl restart docker
```

## run it

**production**:

`docker-compose pull && docker-compose up -d`

### CDN client sample example

Take a look at [`cdn-example`](cdn-example) which has an `index.html` file that illustrate how to use this `cdn-server`.

***keep CALMS and having fun***
