'use strict';

const express = require('express');

// Constants
const PORT = 8080;

// App
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// vendors static
app.use('/', express.static('node_modules'))
//app.use('/angular', express.static('node_modules/angular'))
//app.use('/font-awesome', express.static('node_modules/font-awesome'))
//app.use('/bootstrap', express.static('node_modules/bootstrap'))

app.get('/', function (req, res) {
  res.send('NodeJS CDN vendors\n');
});

app.listen(PORT);
console.log('Running on PORT: [' + PORT + ']');
